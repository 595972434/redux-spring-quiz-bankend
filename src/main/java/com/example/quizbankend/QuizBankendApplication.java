package com.example.quizbankend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizBankendApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizBankendApplication.class, args);
	}

}

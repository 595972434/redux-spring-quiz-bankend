package com.example.quizbankend.controller;

import com.example.quizbankend.domain.Product;
import com.example.quizbankend.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*")
public class ProductController {
    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/products")
    public ResponseEntity getProduct() {
        List<Product> all = productRepository.findAll();
        return ResponseEntity.status(201).body(all);
    }

    @PostMapping("/product")
    public ResponseEntity postProduct(@RequestBody Product product) {
        Product productByProductName = productRepository.findProductByProductName(product.getProductName());
        if (productByProductName == null) {
            productRepository.save(product);
            return ResponseEntity.status(201).body("Create Ok");
        } else {
            return ResponseEntity.status(201).body("Create Fail");
        }
    }
}

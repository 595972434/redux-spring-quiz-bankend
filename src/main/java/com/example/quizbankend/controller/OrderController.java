package com.example.quizbankend.controller;

import com.example.quizbankend.contract.OrderRequest;
import com.example.quizbankend.domain.Order;
import com.example.quizbankend.domain.OrderRepository;
import com.example.quizbankend.domain.Product;
import com.example.quizbankend.domain.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*")
public class OrderController {
    private OrderRepository orderRepository;
    private ProductRepository productRepository;

    public OrderController(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping("/orders")
    public ResponseEntity getOrder(){
        List<Order> orders = orderRepository.findAll();
        return ResponseEntity.status(201).body(orders);
    }

    @PostMapping("/order")
    public ResponseEntity postOrder(@RequestBody OrderRequest orderRequest){
        Product product = productRepository.findProductByProductName(orderRequest.getProductName());
        Optional<Order> order = orderRepository.findByProduct(product);
        return order.map(element->{
            element.setProduct_count(element.getProduct_count()+1);
            orderRepository.save(element);
            return ResponseEntity.status(201).build();
                }
        ).orElseGet(
                ()->{
                    orderRepository.save(new Order(1L,product));
                    return ResponseEntity.status(201).build();
                }
        );

    }

}

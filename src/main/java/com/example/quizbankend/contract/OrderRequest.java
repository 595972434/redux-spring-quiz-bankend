package com.example.quizbankend.contract;

import java.math.BigDecimal;

public class OrderRequest {
    private String productName;

    private BigDecimal productPrice;

    private String productUnit;

    public OrderRequest(String productName, BigDecimal productPrice, String productUnit) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productUnit = productUnit;
    }

    public OrderRequest() {
    }

    public String getProductName() {
        return productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public String getProductUnit() {
        return productUnit;
    }
}

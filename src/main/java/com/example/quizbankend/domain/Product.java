package com.example.quizbankend.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "product")
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "productName",nullable = false)
    private String productName;

    @Column(name="productPrice",nullable = false)
    private BigDecimal productPrice;

    @Column(name="productUnit",nullable = false)
    private String productUnit;

    @Column(name="productImage",nullable = false)
    private String productImage;

    public Product(String productName, BigDecimal productPrice, String productUnit, String productImage) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.productUnit = productUnit;
        this.productImage = productImage;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public String getProductUnit() {
        return productUnit;
    }

    public String getProductImage() {
        return productImage;
    }
}

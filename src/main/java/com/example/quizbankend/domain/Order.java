package com.example.quizbankend.domain;

import javax.persistence.*;

@Table(name = "orders")
@Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long product_count;

    @JoinColumn(name = "product_id")
    @OneToOne
    private Product product;

    public Order(Long product_count, Product product) {
        this.product_count = product_count;
        this.product = product;
    }

    public Order() {

    }

    public Long getId() {
        return id;
    }

    public Long getProduct_count() {
        return product_count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setProduct_count(Long product_count) {
        this.product_count = product_count;
    }
}

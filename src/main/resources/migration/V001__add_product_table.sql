CREATE TABLE IF NOT EXISTS product (
    id BIGINT AUTO_INCREMENT,
    product_name VARCHAR(255) NOT NULL,
    product_price DOUBLE NOT NULL,
    product_unit VARCHAR(255) NOT NULL,
    product_image VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

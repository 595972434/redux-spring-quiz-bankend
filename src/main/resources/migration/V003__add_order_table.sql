CREATE TABLE IF NOT EXISTS orders (
    id BIGINT AUTO_INCREMENT,
    product_count BIGINT NOT NULL,
    product_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    foreign key(product_id) references product(id)
);

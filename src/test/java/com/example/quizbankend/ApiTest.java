package com.example.quizbankend;

import com.example.quizbankend.contract.OrderRequest;
import com.example.quizbankend.domain.Order;
import com.example.quizbankend.domain.OrderRepository;
import com.example.quizbankend.domain.Product;
import com.example.quizbankend.domain.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
public class ApiTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    void should_return_all_product() throws Exception {
        productRepository.save(new Product("cool1", BigDecimal.valueOf(1.5), "bottle", "url"));
        productRepository.save(new Product("cool2", BigDecimal.valueOf(1.5), "bottle", "url"));
        productRepository.save(new Product("cool3", BigDecimal.valueOf(1.5), "bottle", "url"));
        mockMvc.perform(get("/api/products")).andExpect(status().is(201))
                .andExpect(jsonPath("$[0].productName").value("cool1"))
                .andExpect(jsonPath("$[1].productName").value("cool2"))
                .andExpect(jsonPath("$[2].productName").value("cool3"));
    }

    @Test
    void should_post_product_if_no_exist() throws Exception {
        Product product = new Product("cool4", BigDecimal.valueOf(1.5), "bottle", "url");
        String s = new ObjectMapper().writeValueAsString(product);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON_UTF8).content(s))
                .andExpect(status().is(201))
                .andExpect(content().string("Create Ok"));
    }

    @Test
    void should_return_fail_if_product_exist() throws Exception {
        productRepository.save(new Product("cool4", BigDecimal.valueOf(1.5), "bottle", "url"));
        Product product = new Product("cool4", BigDecimal.valueOf(1.5), "bottle", "url");
        String s = new ObjectMapper().writeValueAsString(product);
        mockMvc.perform(post("/api/product").contentType(MediaType.APPLICATION_JSON_UTF8).content(s))
                .andExpect(status().is(201))
                .andExpect(content().string("Create Fail"));
    }


    @Test
    void should_return_all_orders() throws Exception {
        Product save = productRepository.save(new Product("cool1", BigDecimal.valueOf(1.5), "bottle", "url"));
        Product save1 = productRepository.save(new Product("cool2", BigDecimal.valueOf(1.5), "bottle", "url"));
        Order save3 = orderRepository.save(new Order(2L, save));
        Order save4 = orderRepository.save(new Order(3L, save1));
        mockMvc.perform(get("/api/orders")).andExpect(status().is(201))
                .andExpect(jsonPath("$[0].product_count").value(2L))
                .andExpect(jsonPath("$[0].product.productName").value("cool1"));
    }
    @Test
    void should_post_product_to_orders() throws Exception {
        Product save = productRepository.save(new Product("cool1", BigDecimal.valueOf(1.5), "bottle", "url"));
        Product save1 = productRepository.save(new Product("cool2", BigDecimal.valueOf(1.5), "bottle", "url"));
        OrderRequest orderRequest = new OrderRequest("cool1", BigDecimal.valueOf(1.5), "bottle");
        Order save3 = orderRepository.save(new Order(2L, save));
        Order save4 = orderRepository.save(new Order(3L, save1));
        String s = new ObjectMapper().writeValueAsString(orderRequest);
        mockMvc.perform(post("/api/order").content(s).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().is(201));
        Optional<Order> cool1 = orderRepository.findByProduct(productRepository.findProductByProductName("cool1"));
        assertEquals("3",cool1.get().getProduct_count().toString());
    }

    @Test
    void should_create_new_order_when_post_new_product() throws Exception {
        Product save = productRepository.save(new Product("cool1", BigDecimal.valueOf(1.5), "bottle", "url"));
        Product save1 = productRepository.save(new Product("cool2", BigDecimal.valueOf(2.5), "bottle", "url"));

        OrderRequest orderRequest = new OrderRequest("cool2", BigDecimal.valueOf(1.5), "bottle");

        Order save3 = orderRepository.save(new Order(2L, save));
        String s = new ObjectMapper().writeValueAsString(orderRequest);
        mockMvc.perform(post("/api/order").content(s).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().is(201));
        Optional<Order> cool1 = orderRepository.findByProduct(productRepository.findProductByProductName("cool1"));
        Optional<Order> cool2 = orderRepository.findByProduct(productRepository.findProductByProductName("cool2"));
        assertEquals("2",cool1.get().getProduct_count().toString());
        assertEquals("1",cool2.get().getProduct_count().toString());
    }

}
